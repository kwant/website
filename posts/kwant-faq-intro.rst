.. title: Kwant adds an FAQ to the documentation
.. slug: kwant-faq-intro
.. date: 2017-06-29 07:07:23 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

The Kwant team in Grenoble is currently hosting a masters student, Paul Clisson,
who has taken the initiative and written a "Frequently Asked
Questions" (FAQ) section to complement the existing Kwant tutorial; thanks Paul!

.. TEASER_END

A preliminary version of of the FAQ is already available
`here <https://test.kwant-project.org/doc/doc-faq/tutorial/FAQ>`_. Any and all
user feedback is valuable to us, so tell us what you think! You can either join
the `discussion on the mailing
list <https://www.mail-archive.com/kwant-discuss@kwant-project.org/msg01313.html>`_
or comment on the open `merge
request <https://gitlab.kwant-project.org/kwant/kwant/merge_requests/148>`_ on
the Kwant gitlab.

Happy Kwanting,

Kwant team
