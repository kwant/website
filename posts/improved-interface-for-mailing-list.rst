.. title: Improved web interface for the mailing list
.. slug: improved-interface-for-mailing-list
.. date: 2020-05-28 20:00:00 UTC
.. category: 
.. type: text

The Kwant mailing list has been migrated to a new server that provides a much-improved web interface.

.. TEASER_END

In May 2020, thanks to the friendly people at python.org,
kwant-discuss was migrated to a server running Mailman 3.
The list remains a mailing list (existing subscriptions continue),
but now also offers a modern web interface that is similar to a web forum.
The complete archives were migrated and are now searchable.
Please see the `“community” section </community>`_ of this site for usage information.
The `old address of the list <kwant-discuss@kwant-project.org>`_ remains usable: any messages are simply redirected to the new one.
