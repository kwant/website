.. title: List of Kwant extensions added to website
.. slug: kwant-extensions
.. date: 2018-03-28 11:56:55 UTC
.. tags:
.. category:
.. link:
.. description:
.. type: text

We have added a `new section to the website </extensions.html>`_
that showcases useful extensions to Kwant that have been made by members of the
community.

.. TEASER_END

While we welcome contributions to the core Kwant package, we realize that
it can be a lot of work to implement something that is both robust enough
and of sufficiently general interest to be added to Kwant directly.

Nevertheless there is a body of existing software that extends Kwant in ways
that could be useful to others in the Kwant community. We hope that providing
a central place for listing these extensions will make them more discoverable
by others.

If you have extended Kwant in some way, please get in touch via the
`kwant-devel <mailto:kwant-devel@kwant-project.org>`_ mailing list!

Happy Kwanting,

Kwant team
