.. title: Release of Kwant 1.4
.. slug: release-of-kwant-14
.. date: 2019-03-06 11:46:18 UTC
.. category: release-announcement
.. type: text

We are pleased to announce the release of Kwant 1.4, the result of a year and a half of development.

.. TEASER_END

Highlights of the new release:

* Adding magnetic field to systems, even in complicated cases, is now specially supported.
* The KPM module can now calculate conductivities.
* The `Qsymm library <https://gitlab.kwant-project.org/qt/qsymm>`_ for Hamiltonian symmetry analysis has been integrated.
* The handling of system parameters has been improved and optimized.
* Plotting has been improved, most notably through the addition of a routine that plots densities with interpolation.
* Installing Kwant on Windows is now much easier thanks to Conda packages.

All these and other improvements that are detailed in the `what's new in Kwant 1.4 </doc/1/pre/whatsnew/1.4>`_ document.  The new version is practically 100% backwards-compatible with scripts written for previous versions of Kwant 1.x.

The following people contributed code to this release:

* Anton Akhmerov
* Paul Clisson
* Christoph Groth
* Thomas Kloss
* Bas Nijholt
* Pablo Pérez Piskunow
* Tómas Örn Rosdahl
* Rafał Skolasiński
* Dániel Varjas
* Joseph Weston

We would like to hear your feedback at kwant-discuss@kwant-project.org.
