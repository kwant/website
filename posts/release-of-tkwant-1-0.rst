.. title: Release of TKwant 1.0
.. slug: release-of-tkwant-1-0
.. date: 2020-09-08 15:00:00 UTC
.. category: release-announcement
.. type: text

We are pleased to announce the first release of TKwant,
a Kwant extension for time-resolved quantum transport.

.. TEASER_END

A system is defined in TKwant in the same way as in Kwant except that some terms in the Hamiltonian may depend on the time parameter. Any system that can be studied in Kwant can be studied with TKwant as well. Typical problems where TKwant can be used include the propagation of voltage pulses, spectroscopy of interferometers, Floquet topological insulators, time resolved Andreev reflection and more.

An introduction to TKwant can be found in `TKwant paper <https://arxiv.org/abs/2009.03132>`_. The software itself and
its tutorials are available on the `TKwant website <https://tkwant.kwant-project.org/>`_.
