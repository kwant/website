================================
Installing Kwant
================================

License and citation request
============================
Kwant is free software covered by the `2-clause BSD license </license>`_.

If you have used Kwant for work that has lead to a scientific publication,
please `cite the Kwant paper and possibly other relevant publications
</cite>`_.

Prerequisites
=============
In order to use Kwant you will need to install a distribution of the Python
language. If you are using a GNU/Linux operating system this should already be
installed on your computer.

If you are using Mac OS X or Microsoft Windows you will need to install a
Python distribution yourself. Details of how to do this are in the
installation instructions of the corresponding platform.

**Kwant version 1.2 and newer requires at least Python 3.4**.

Those who must use Python 2 can still use Kwant up to version 1.1,
which will receive bug fixes for several years
after 2015 (but no new features).

The instructions below assume Python 3.  They should be also valid for Python
2 if all occurrences of ``python3``, ``pip3``, etc. are replaced by
``python``, ``pip``.


Platforms
=========
Installation instructions are available for the major operating systems:

+ `GNU/Linux`_
+ `Mac OS X`_
+ `Microsoft Windows`_
+ `Building from source`_

GNU/Linux
=========
Pre-built packages exist for the following distributions:

+ `Debian <#debian-and-derivatives>`_
+ `Ubuntu <#ubuntu-and-derivatives>`_
+ `Arch Linux`_

We also provide `Conda packages <#conda>`_ for users of the `Anaconda
<https://www.continuum.io/downloads>`_ Python distribution. This is a useful
option if you do not have root privileges on the machine where you would like
to install Kwant (e.g. on a computing cluster).

If your distribution is not listed above, and you do not want to use the
Conda packages, you can always install Kwant using `pip`_. Or by directly
building `from source <#building-from-source>`_.


Debian and derivatives
----------------------
The easiest way to install Kwant on a Debian system is using the pre-built
packages we provide.  We target Debian "stable", but our packages may also work
on many other recent Debian-derived distributions as well.

The following instructions will install Kwant on Debian stable "buster".
They need to be executed as root.

1. Add the following lines to ``/etc/apt/sources.list``::

       deb http://ftp.debian.org/debian buster main
       deb http://downloads.kwant-project.org/debian/ buster main
       deb-src http://downloads.kwant-project.org/debian/ buster main

2. (Optional) Add the OpenPGP key used to sign the repositories by executing::

       apt-key adv --keyserver keyserver.ubuntu.com --recv-key C3F147F5980F3535

   The fingerprint of the key is 5229 9057 FAD7 9965 3C4F 088A C3F1 47F5 980F
   3535.

3. Update the package data, and install Kwant::

       apt-get update
       apt-get install python3-kwant


Ubuntu and derivatives
----------------------
The easiest way to install Kwant on an Ubuntu system is using the pre-built
packages we provide.  Execute the following commands in a terminal::

    sudo apt-add-repository -s ppa:kwant-project/ppa
    sudo apt-get update
    sudo apt-get install python3-kwant

This should provide up-to-date Kwant for all recent versions of Ubuntu,
up to at least the last LTS version.


Debian or Ubuntu: building packages from source
-----------------------------------------------
It is straightforward to build Debian/Ubuntu packages from source.  This can be
useful when the installation from pre-built packages has failed for some
reason (e.g. for non-x86 CPUs).

As a prerequisite, a ``deb-src`` entry for Kwant is needed in
``/etc/apt/sources.list``.  (It will be present if either of the above
instructions for Debian or for Ubuntu have been followed.)

FIrst, install the build dependencies (as root)::

    apt-get build-dep tinyarray kwant

Then, compile Tinyarray and Kwant.  This may be done as a normal user. ::

    cd /tmp
    apt-get source --compile tinyarray
    apt-get source --compile kwant

Finally, install the built packages (again as root)::

    dpkg --install python3-tinyarray_*.deb
    dpkg --install python3-kwant_*.deb


Arch Linux
----------
`Arch install scripts for Kwant
<https://aur.archlinux.org/packages/python-kwant/>`_ are kindly provided by
Jörg Behrmann (formerly by Max Schlemmer).  To install, follow the `Arch User
Repository installation instructions
<https://wiki.archlinux.org/index.php/Arch_User_Repository#Installing_packages>`_.
Note that for checking the validity of the package you need to add the key
used for signing to your user's keyring via::

    gpg --keyserver pool.sks-keyservers.net --recv-key C3F147F5980F3535

The fingerprint of the key is 5229 9057 FAD7 9965 3C4F 088A C3F1 47F5 980F
3535.


Mac OS X
========
`Pre-built packages <#conda>`_ for Max OS X exist for the Conda package
manager, which is a part of the ``Anaconda`` Python distribution.
Using Conda is the recommended way to install Kwant on Mac OS X.

If you do not want to use the Conda packages, you can always install Kwant
using `pip`_. Or by directly `building from source`_.

We previously maintained Homebrew and Macports packages for Kwant, but due to
effort required to keep them up to date we have dropped support for these
installation methods. We recommend that people use the Conda packages
whenever possible.


Microsoft Windows
=================
`Pre-built packages <#conda>`_ for Microsoft Windows exist for the Conda
package manager, which is a part of the ``Anaconda`` Python distribution.
Using Conda is the recommended way to install Kwant on Microsoft Windows.

We previously recommended using that Kwant packages built by
Christoph Gohlke, however due to the complexity of the installation
instructions the Conda packages are preferred. Installation of the
package provided by Christoph Gohlke is only needed if the integration
with MUMPS is required (as this is still not available for the Conda packages).

The instructions for installing the alternative package are included below

Alternative Package
-------------------
The following instructions explain how to install the official version
of Python 3, Kwant, and its dependencies.

1. Determine whether you have a 32-bit or 64-bit Windows installation by
   following these `instructions <http://support.microsoft.com/kb/827218>`_.

2. Download and install Python 3 for the appropriate architecture (32-bit: “x86” or
   64-bit: “x86-64”) from the official `Python download site for Windows
   <http://www.python.org/download/windows>`_. The current stable version
   at the time of writing is Python 3.6.

3. Open a command prompt, as described in "How do I get a command prompt" at
   the `Microsoft Windows website <http://windows.microsoft.com/en-us/windows/command-prompt-faq>`_.

4. In the command prompt window, execute::

        C:\Python36\python.exe C:\Python36\Tools\Scripts\win_add2path.py

   (Instead of typing this command, you can also just copy it from here and
   paste it into the command prompt window). If you did not use the default
   location to install Python in step 2, then replace ``C:\Python36`` by the
   actual location where Python is installed.  You may also need to adjust the
   version (“36” signifies Python 3.6).

5. Reboot your computer.

6. Download the necessary packages (with the ending ``.whl``) for your
   operating system (32 or 64 bit) and Python version (e.g. ``cp35`` for
   Python 3.5) from the website of `Christoph Gohlke
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/>`_.  For Kwant, we recommend to
   download at least `NumPy
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy>`_, `SciPy
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/#scipy>`_, `Matplotlib
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/#matplotlib>`_, `Sympy
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/#sympy>`_, `MPMath
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/#mpmath>`_, `Pytest
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/#pytest>`_, `Six
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/#six>`_, `Tinyarray
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/#tinyarray>`_, and `Kwant
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/#kwant>`_ itself.  Make sure to
   put the downloaded files into a directory without any other ``.whl`` files.

7. Open a command prompt with administrator rights, as described in “How do I
   run a command with elevated permissions” at the `Microsoft Windows website
   <http://windows.microsoft.com/en-us/windows/command-prompt-faq>`_.

   Go to the directory with the ``.whl`` files, e.g.::

       cd c:\Users\YOUR_USERNAME\Downloads

   To install all the ``.whl``-files in the current directory, execute ::

       python -c "import pip, glob; pip.main(['install', '--no-deps'] + glob.glob('*.whl'))"

   The above cryptic command is equivalent to ``pip install --no-deps
   *.whl``, i.e. it installs all the wheel files in the current directory
   using pip.  Because the Windows command interpreter does not support globs,
   we have to rely on the globbing as provided by Python itself.

Now you are done, you can ``import kwant`` from within Python scripts.

(Note that many other useful scientific packages are available in Gohlke’s
repository.  For example, you might want to install `IPython
<http://www.lfd.uci.edu/~gohlke/pythonlibs/#ipython>`_ and its various
dependencies so that you can use the `IPython notebook <http://ipython.org/notebook.html>`_.)

If you do not want to use the Conda packages, you can always install Kwant
using `pip`_. Or by directly `building from source`_.


Conda
=====
Conda is the package manager for the Anaconda Python distribution.

Kwant currently has Conda packages for GNU/Linux, Mac OS X, and Microsoft Windows.

1. Download the Python 3.6 version of `Anaconda <https://www.continuum.io/downloads>`_ for
   your platform and install it.

2. Execute the following command in a terminal::

    conda install -c conda-forge kwant

   The above command installs Kwant and all of its dependencies from the
   ``conda-forge`` channel.


``pip``
=======
.. caution::

    Installing Kwant with ``pip`` is not easy because Kwant has several
    non-Python dependencies and requires a C compiler. These instructions
    are provided for advanced users only.

``pip`` is the standard Python package manager that downloads and installs
packages from the `Python package index <https://pypi.python.org/>`_.

1. Install the non-Python dependencies of Kwant: a C compiler, BLAS, Lapack,
   and (optionally) MUMPS (see `Installing non-Python dependencies`_).

2. Execute the following command in a terminal::

    sudo pip3 install kwant

   The above command installs Kwant and all of its Python dependencies from the
   Python package index.

The latest development build of Kwant can be installed directly from Kwant's
Git repository::

    sudo pip3 install git+https://gitlab.kwant-project.org/kwant/kwant.git

Each of the above commands will perform a system-wide install (to
``/usr/local`` on Unix).  Type ``pip3 help install`` for installation options
and see `pip documentation <https://pip.readthedocs.org/>`_ for a detailed
description of ``pip``.

Installing non-Python dependencies
----------------------------------

As mentioned above, ``pip`` will not install any non-Python dependencies
required by Kwant. Kwant has several non-Python dependencies:

+ a C compiler (e.g. ``gcc``)
+ BLAS
+ Lapack
+ `MUMPS <http://graal.ens-lyon.fr/MUMPS/>`_.

If you using a GNU/Linux system then your distribution probably has packages
for these libraries; you will need to install the `-dev` or `-devel` versions
of the packages.

As an example, on a Debian or Ubuntu system, the following
command will install the non-Python dependencies of Kwant::

    sudo apt-get install build-essential gfortran libopenblas-dev liblapack-dev libmumps-scotch-dev

On Debian or Ubuntu systems the Kwant build scripts should find libraries that
have been installed in the above way automatically.  This will be signaled at
the end of the build process as follows::

    ******************************** Build summary ********************************
    Default LAPACK and BLAS
    Auto-configured MUMPS
    *******************************************************************************

On other platforms it is possible that MUMPS is not linked against Kwant during
installation.  If this is the case the build process must be `configured
manually <doc/1/pre/install.html#build-configuration>`_ by writing a
``build.conf`` file.  You can then tell ``pip`` to use this ``build.conf`` when
installing kwant::

    sudo pip install --global-option="--configfile=/path/to/build.conf" kwant


Building from source
====================
If no packages are available for the system you use, or if you would like to
build Kwant from source for another reason (expert users may want to customize
Kwant to use certain optimized versions of libraries), you can `download
the source code <https://downloads.kwant-project.org/kwant>`_ and consult the
documentation on `how to install Kwant from source <doc/1/pre/install.html>`_.
