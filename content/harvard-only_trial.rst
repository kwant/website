Trial pre-release of Kwant
==========================

With the release of Kwant the instructions that were here were rendered
obsolete. Please go to the `installation instructions </install.html>`_ for the
latest Kwant release.
