The Kwant community
===================

The Kwant project is an international collaboration.
Everybody is welcome to participate in the community
by asking and replying to questions, reporting bugs, providing suggestions,
and `contributing to the code and documentation </contribute.html>`_.

The Kwant community uses three public communication channels:
the mailing list for generic discussions, questions, and announcements;
the gitlab instance for development discussions and reporting bugs;
and the chat for real-time discussions related to development.

A list of `Kwant authors </authors>`_ is included in the documentation.
Please contact the authors directly only for matters that cannot be discussed on the public channels.


Mailing list
------------

The kwant-discuss mailing list is the main public communication platform for anything related to Kwant:
questions, discussions, development, and announcements.
It may be used both as a web forum and as a classical mailing list.

The `kwant-discuss web interface <https://mail.python.org/archives/list/kwant-discuss@python.org/>`_
allows to follow discussions and search the archives.

To start a new discussion thread, you may either use the web interface
(sign-in required)
or write directly to
kwant-discuss@python.org.
In order to avoid spam,
messages of unsubscribed participants are held for moderation.

Similarly, replying is possible either by mail
(if you received the post to which you want reply by mail),
or using the web interface:
the “reply” button under each message allows to compose directly
(for signed-in users),
or triggers a reply by mail (otherwise).

You may subscribe to receive posts to the list by mail.
Subscription is done either through the
`kwant-discuss info page
<https://mail.python.org/mailman3/lists/kwant-discuss.python.org/>`_
or by sending any message to
kwant-discuss-join@python.org.
(The subject and content are ignored and may be empty.)
To unsubscribe, either use the info page,
or send any message to kwant-discuss-leave@python.org.

List etiquette :

- When asking questions,
  `help others to help you <https://stackoverflow.com/help/how-to-ask>`_.
  In particular, use the search function before posting.

- When replying, please avoid quoting the complete original message.
  Instead, consider
  `bottom-posting <https://en.wikipedia.org/wiki/Posting_style#Bottom-posting>`_.


Announcements mailing list
--------------------------

This read-only list is used for important announcements like new releases of Kwant.
Only a few messages are sent per year.
These announcements are also posted on the main mailing list,
so there is no need to subscribe to both lists.
We recommend every user of Kwant to subscribe at least to this list in order to stay informed about new developments.

The `kwant-announce archives <https://mail.python.org/archives/list/kwant-announce@python.org/>`_
are available on the web.

To subscribe, either use the form on the `kwant-announce info page <https://mail.python.org/mailman3/lists/kwant-announce.python.org/>`_
or simply send any message to kwant-announce-join@python.org.
(The subject and content are ignored and may be empty.)
To unsubscribe, either use the info page, or send any message to kwant-announce-leave@python.org.


Development chat
----------------

The Kwant developer chat is accessible to the community via `Gitter <https://gitter.im/kwant-project/Lobby>`_.
Come say hi if you would like to get involved with developing Kwant!


Gitlab instance
---------------

The Kwant project runs a `gitlab instance <https://gitlab.kwant-project.org/>`_
that hosts the `main Kwant code repository <https://gitlab.kwant-project.org/kwant/kwant>`_
as well as the `repositories of related projects <https://gitlab.kwant-project.org/kwant>`_.
The gitlab instance is used for reporting bugs (see next section) and `development </contribute.html>`_


Reporting bugs
--------------

If you encounter a problem with Kwant,
first try to reproduce it with as simple a system as possible.
Double-check with the documentation that what you observe is actually a bug in Kwant.
If you think it is, please check `the list of known bugs in Kwant <https://gitlab.kwant-project.org/kwant/kwant/issues?label_name=bug>`_.
It may be also a good idea to search or ask on the general mailing list.
(You can use the search box at the top of this page.)

If after checking you still think that you have found a bug, please add it to
the above-mentioned list of bugs by creating an issue with the “bug” label.  A
useful bug report should contain:

- The versions of software you are using: Kwant, Python, operating system, etc.

- A description of the problem, i.e. what exactly goes wrong.  This should
  include any relevant error messages.

- Enough information to reproduce the bug, preferably in the form of a simple
  script.
