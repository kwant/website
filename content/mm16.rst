APS March meeting 2016 tutorial
-------------------------------

This page collects materials for the APS March meeting 2016 tutorial “Introduction to Computational Quantum Nanoelectronics” (`announcement <http://www.aps.org/meetings/march/events/tutorials.cfm#t10>`_).
The tutorial consists of a set of `Jupyter <http://jupyter.org/>`_ notebooks that combine demonstrations and explainations with exercises.  These materials are made available under a `simplified BSD license <https://gitlab.kwant-project.org/kwant/kwant-tutorial-2016/blob/master/LICENSE>`_.

* `Run the tutorial directly in your web browser <http://mybinder.org/repo/kwant-project/kwant-tutorial-2016/>`_.  The Python environment and Kwant will run on a server provided by the `Binder service <http://mybinder.org/>`_.
* `Download everything <https://gitlab.kwant-project.org/kwant/kwant-tutorial-2016/repository/archive.zip?ref=master>`_ as a single zip archive.  Kwant 1.2 or newer is required to execute the notebooks.
* `Browse the git repository <https://gitlab.kwant-project.org/kwant/kwant-tutorial-2016>`_.
